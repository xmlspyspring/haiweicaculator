var HaiweiCaculator = function (option) {
    var opt = {
        totalBaseClassName: "total_",
        averageBaseClassName: "average_",
        onRead: function (targetElement, targetElements) {
            var val = targetElement.innerText;
            return Number(val);
        },
        onWrite: function (caculatedValue, element) {
            element.innerText = caculatedValue;
        }
    };

    if (isExitsVariable(option)) {
        if (isExitsVariable(option.totalBaseClassName)) {
            opt.totalBaseClassName = option.totalBaseClassName;
        }
        if (isExitsVariable(option.averageBaseClassName)) {
            opt.averageBaseClassName = option.averageBaseClassName;
        }
        if (isExitsFunction(option.onRead)) {
            opt.onRead = option.onRead;
        }
        if (isExitsFunction(option.onWrite)) {
            opt.onWrite = option.onWrite;
        }
    }

    this.option = opt;
    window.console.debug("option is: %o", this.option);

    this.caculateTotal = function () {
        var className = opt.totalBaseClassName;
        caculate(className, function (valueArray) {
            var result = 0;
            for (var i = 0; i < valueArray.length; i++) {
                result = result + valueArray[i];
            }
            return result;
        });
    };

    this.caculateAverage = function () {
        var className = opt.averageBaseClassName;
        caculate(className, function (valueArray) {
            var result = 0;
            var len = valueArray.length;
            for (var i = 0; i < len; i++) {
                result = result + valueArray[i];
            }
            return result / len;
        });
    };

    function caculate(baseClassName, caculateFunction) {
        var elements = document.getElementsByClassName(className);
        for (var j = 0; j < elements.length; j++) {
            var element = elements[j];
            var classList = element.classList;
            for (var i = 0; i < classList.length; i++) {
                if (classList[i] == className) {
                    var targetClassName = classList[i + 1]; //被计算的元素class名称
                    if (!targetClassName) {
                        continue;
                    }

                    var targetElements = document.getElementsByClassName(targetClassName);
                    var valueArray = [];
                    for (var k = 0; k < targetElements.length; k++) {
                        var targetElement = targetElements[k];
                        if (targetElement.className.includes(className + " ")) {
                            continue;
                        }
                        var val = opt.onRead(targetElement, targetElements);
                        valueArray.push(val);
                    }
                    var caculatedValue = caculateFunction(valueArray);
                    opt.onWrite(caculatedValue, element);
                    i++;
                }
            }
        }
    }

    function isExitsFunction(functionName) {
        try {
            if (typeof (eval(functionName)) == "function") {

                return true;
            }
        } catch (e) {}
        return false;
    }

    function isExitsVariable(variableName) {
        try {
            if (typeof (variableName) == "undefined") {
                return false;
            } else {
                return true;
            }
        } catch (e) {}
        return false;
    }
};